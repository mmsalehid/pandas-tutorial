import datetime

import pandas as pd
from matplotlib import style
import pandas_datareader as web
import matplotlib.pyplot as plt

style.use('ggplot')

start = datetime.datetime(2010, 1, 1)
end = datetime.datetime(2016, 1, 1)

df = web.DataReader("XOM", "yahoo", start, end)

print(df.head())

df['Adj Close'].plot()

plt.show()