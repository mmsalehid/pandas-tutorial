from matplotlib import style
import pandas as pd
import numpy as np
style.use('ggplot')

web_stats = {'Day': [1, 2, 3, 4, 5, 6],
             'Visitors': [43, 67, 52, 31, 75, 59],
             'Bounce_Rate': [52, 60, 58, 64, 71, 66]}

df = pd.DataFrame(web_stats)

#print(df)
#print(df.head())
#print(df.tail())
#print(df.head(3))

print(df.set_index('Day'))
#df = df.set_index('Day')
#df.set_index('Day', inplace=True)
print(df.head())

print(df['Visitors'])
print(df.Bounce_Rate)
print(df[['Visitors', 'Bounce_Rate']])
#print(df.Visitors.tolist())
print(np.array(df[['Visitors', 'Bounce_Rate']]))
df2 = (pd.DataFrame(np.array(df[['Visitors', 'Bounce_Rate']])))
print(df2)
